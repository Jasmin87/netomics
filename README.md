# NetOmics in a Nutshell #

NetOmics is a user-friendly application to associate high dimensional time course `omics' data from a single or multiple molecular levels. NetOmics enables to:

1.  Associate pairwise molecule trajectories within a single data set or across two data sets,
2.  Visualise the pairwise identified associations with or without estimated delays,
3.  Visualise the associations in a network.


## Quick start ##
### What do I need to run NetOmics? ###
NetOmics version 1.0 functionality was tested on R version 3.2 and RStudio version 0.99. 
Follow the subsequent steps to install R (>= 3.2), RStudio (>= 0.99) and NetOmics:

1. Download and install the latest version of R for your machine from [here](https://cran.r-project.org/bin/windows/base/).
2. Download and install the latest version of  **RStudio Desktop** for your machine from [here](https://www.rstudio.com/products/rstudio/#Desktop).
3. Once you have R and RStudio running, download NetOmics from Bitbucket [here](https://bitbucket.org/Jasmin87/netomics)
4. You will also need to install the R package shiny in order to run NetOmics. Open RStudio and type into the RStudio console:

~~~~
install.packages('shiny')
~~~~


### Run NetOmics ###
Run NetOmics by typing the following commands in the console:

    library(shiny)
    runApp('C:/filepath/to/NetOmics')
    

**Note:** The first time you launch NetOmics may take time as many package dependencies need to be  automatically installed. 


### Who do I talk to? ###
The examples will hopefully answer most questions about NetOmics. However, additional questions can be directed to j.straube[at]qfab.org.
We appreciate bug reports in the software or R functions and welcome any suggestions or comments for improvements.