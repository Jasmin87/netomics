
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script type='text/javascript' src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"> </script>
<script type='text/javascript' src="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"> </script>

<style>

.node {
  stroke: #fff;
  stroke-width: 1.5px;
}

  
.link {
  stroke: #999;
  stroke-opacity: .5;
}

.link:hover {
  stroke-dasharray: 10,2;
  stroke-opacity: 1;
  stroke-width: 2px;
}



path:hover {
  fill:red;
  stroke: red;
  stroke-width: 2px;
}


marker{
    fill: #333;
}



</style>

<script type="text/javascript">var networkOutputBinding = new Shiny.OutputBinding();
  $.extend(networkOutputBinding, {
    find: function(scope) {
      return $(scope).find('.shiny-network-output');

    },
    renderValue: function(el, data) {


   
      //format nodes object
      var linkedByIndex = {};
      var nodes = [];
      // get data object containing type name 
     for (var i = 0; i < data.names.length; i++){
        nodes.push({"name": data.names[i],
        "type":data.type[i],'connIn':data.connIn[i],'connOut':data.connOut[i],'connEq':data.connUndef[i]});
        
     linkedByIndex[i + "," + i] = 1;
      }


      var lin = new Array();
      for (var i = 0; i < data.links.source.length; i++){
        lin.push({"source": data.links.source[i],"target": data.links.target[i],"weight":data.links.weight[i], "delay":data.links.delay[i]})
         
          linkedByIndex[data.links.source[i] + "," + data.links.target[i]] = 1;
      }

  
      var toggle = 0;
  
var width = window.innerWidth*0.43 ;

//var height = window.innerHeight;
//var width = 800;
var height = window.innerHeight*0.8; 
      var color = d3.scale.category10();
      var min_zoom = 0.3;
      var max_zoom = 7;


      var force = d3.layout.force()
        .size([width, height])
          .nodes(nodes)
          .links(lin)
        //.group(g)
          //A negative value results in node repulsion, while a positive value results in node attraction
        .charge(-120)
        .linkDistance(function(d){if(d.delay==0){return 50;}else if(Math.abs(d.delay)<4){return 10*(Math.abs(d.delay)+1)+50;}else{return 100;};})
         .linkStrength(function(d){return Math.abs(d.weight);})
       //  .linkDistance(50)
        .size([width, height])
       // .on("tick", tick)

        //Gravity to center [0,1], 0 no gravity to center
        .gravity(0.1);
        //.start();
        //.end();

        
///Drag connected nodes
      var drag = force.drag()
        .on("dragstart", dragstart);
      
      //remove the old graph
      var svg = d3.select(el).select("svg");
      svg.remove();
      
      $(el).html("");
      
      
      var zoom = d3.behavior.zoom();
          zoom.scaleExtent([min_zoom,max_zoom]);
     
      //append a new one
    var svg = d3.select(el).append("svg")
        .attr("width", width)
        .attr("height", height)
        .style("pointer-events", "all")
      .append("svg:g")
        .call(zoom)
      .append('svg:g');
      
      zoom.on("zoom", redraw);

        
   
    ///////// ZOOM ///////////
  function redraw() {
      svg.attr("transform",
          "translate(" + d3.event.translate + ")"
          + " scale(" + d3.event.scale + ")");
      }

     //////// Link between nodes ///////////
   
  //Source http://stackoverflow.com/questions/28050434/introducing-arrowdirected-in-force-directed-graph-d3
  
  ////////Define the marker //////////
    svg.append("svg:defs").selectAll("marker")
      .data(["suit", "licensing", "resolved"])
 //   .data(["end"])      // Different link/path types can be defined here
      .enter().append("svg:marker")    // This section adds in the arrows
        .attr("id", function(d) { return d; })
    //.attr("id", String)
        .attr("viewBox", "0 -5 10 10")
        // offset marker to make them point on the symbol edge
        .attr("refX", 25)
   // .attr("refY", -5)
         .attr("markerWidth", 6)
         .attr("markerHeight", 6)
         .attr("orient", "auto")
         .append("svg:path")
            .attr("d", "M0,-5L10,0L0,5");
    

   //Essential shows link but no marker
       var link = svg.selectAll("line.link")
          .data(lin)
          .enter().append("line")
          .attr("class", "link")
          /// add defined marker end
          .attr("marker-end", "url(#suit)")
          // stroke color in dependency of positive / negative correlation 
          .style("stroke", function(d) {if(d.weight<0){return "#aec7e8"; }else{return "#ffbb78";}})
          // stroke opacity in dependency of correlation 
          .style("opacity", function(d) { return Math.abs(d.weight)});
          link.append("title")
          .text(function(d){return "Correlation: "+d.weight+"\nDelay: " + d.delay}); 
     
//  var path = svg.append('svg:g').selectAll('path');
//      path = path.data(lin);
 
//////////   NODES //////////////
  var gnodes = svg.selectAll('g.gnode')
        .data(nodes)
        .enter()
        .append('g')
        .classed('gnode', true)
        // only display connected nodes on click
        .on('click',connectedNodes);


 
// Create nodes colors and shape depending on what type they are M or T 
 var node = gnodes.append("path")
      .attr("class", "node")
      .attr("d", d3.svg.symbol()
      .type(function(d) { if(d.type=="T"){return 'circle'; }else{return 'square';};})
      .size(function(d) {if((d.connIn + d.connOut + d.connEq)<200){return 200;}else{return (d.connIn + d.connOut + d.connEq);};}))
      .style("fill", function(d) {if(d.type=="T"){return "#1f77b4"; }else{return "#7f7f7f";}})
      .style("opacity", 0.9)
      .call(force.drag);
      // text on mouse over
      node.append("title")
          .text(function(d){return "Name: "+d.name+"\nConnections \nIncoming: " + d.connIn 
          + "\nOutgoing: " + d.connOut + "\nEqual: " + d.connEq}); 

  function tick(e) {
   link.attr("x1", function(d) { return d.source.x; })
       .attr("y1", function(d) { return d.source.y; })
       .attr("x2", function(d) { return d.target.x; })
       .attr("y2", function(d) { return d.target.y; });
   node.attr("cx", function(d) { return d.x; })
       .attr("cy", function(d) { return d.y; });
    node.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";});
      }


var n = 100
var loading = svg.append("text")
    .attr("x", width / 2)
    .attr("y", height / 2)
    .attr("dy", ".35em")
    .style("text-anchor", "middle")
    .text("Simulating. One moment please…");

// Use a timeout to allow the rest of the page to load first.
//setImmediate(function() {
setTimeout(function() {
  // Run the layout a fixed number of times.
  // The ideal number of times scales with graph complexity.
  // Of course, don't run too long—you'll hang the page!
   var safety = 0;
   force.start();
while(force.alpha() > 0.05) { // You'll want to try out different, "small" values for this
    force.tick();
    if(safety++ > 100) {
        force.stop();
    }}
//  force.resume();
//  for (var i = n * n; i > 0; --i) force.tick();

    
  link.attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  node.attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; });
  node.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";});
      

  loading.remove();
}, 10);



 //         safety = 0;
//while(force.alpha() > 0.05) { // You'll want to try out different, "small" values for this
//    force.tick();
//    if(safety++ > 500) {
//      break;// Avoids infinite looping in case this solution was a bad idea
//    }
//}


//var stepsPerFrame = 5;

//requestAnimationFrame(function frame(){
//    force.resume();
//    for (var i = stepsPerFrame; i > 0; --i) force.tick();
//    force.stop();

//    if(!endCondition)
// requestAnimationFrame(frame);
//});

      function dragstart(d) {
        d3.select(this).classed("fixed", d.fixed = true);
      }

       

//This function looks up whether a pair are neighbours
        function neighboring(a, b) {
              return linkedByIndex[a.index + "," + b.index];
          }
      
//http://stackoverflow.com/questions/30634168/three-js-graph-visualization-get-connected-nodes-when-click-on-an-object

//function to change opacityof nodes on double click
function connectedNodes() {
    if (toggle == 0) {
        //Reduce the opacity of all but the neighbouring nodes
        d = d3.select(this).node().__data__;
        node.style("opacity", function (o) {
            return neighboring(d, o) | neighboring(o, d) ? 1 : 0.1;
        });
        link.style("opacity", function (o) {
            return d.index==o.source.index | d.index==o.target.index ? 1 : 0.1;
        });
        //Reduce the op
        toggle = 1;
     
    } else {
        //Put them back to opacity=1
        node.style("opacity", 1);
        link.style("opacity", 1);
      //  toggle = 0;
         d3.select(this).classed("fixed", d.fixed = false);
        toggle = 0;
        
    }
     
}


    }
  });


  
  Shiny.outputBindings.register(networkOutputBinding, 'trestletech.networkbinding');
  
  </script>