<style>

.node {
  cursor: pointer;
  stroke: #3182bd;
  stroke-width: 1.5px;
}

.link {
  fill: none;
  stroke: #9ecae1;
  stroke-width: 1.5px;
}

</style>

<script src="http://d3js.org/d3.v3.min.js"></script>
<script type="text/javascript">var networkOutputBinding = new Shiny.OutputBinding();
  $.extend(networkOutputBinding, {
    find: function(scope) {
      return $(scope).find('.shiny-network-output');
    },
    renderValue: function(el, data) {
      
var width = 960,
    height = 500,
    root;
var nodes = new Array();
    for (var i = 0; i < data.names.length; i++){
        nodes.push({"name": data.names[i]})
      }

      var lin = data.links
      var force = d3.layout.force()
        .nodes(nodes)
        .links(lin)
        .charge(-120)
        .linkDistance(30)
        .size([width, height])
        .start();
        .on("tick", tick);
        .on("dblclick", dblclick);
        .call(drag);
    
          //remove the old graph
      var svg = d3.select(el).select("svg");
      svg.remove();
      
      $(el).html("");
      
      //append a new one
      svg = d3.select(el).append("svg");

    .attr("width", width)
    .attr("height", height);

var link = svg.selectAll(".link"),
    node = svg.selectAll(".node");

d3.json("readme.json", function(json) {
  root = json;
  update();
});

force.on("update", function() {
  var nodes = flatten(root),
      links = d3.layout.tree().links(nodes);

  // Restart the force layout.
  force
      .nodes(nodes)
      .links(links)
      .start();

  // Update the links.
  link = link.data(links, function(d) { return d.target.id; });

  // Exit any old links.
  link.exit().remove();

  // Enter any new links.
  link.enter().insert("line", ".node")
      .attr("class", "link")
      .attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  // Update the nodes.
  node = node.data(nodes, function(d) { return d.id; }).style("fill", color);

  // Exit any old nodes.
  node.exit().remove();

  // Enter any new nodes.
  node.enter().append("circle")
      .attr("class", "node")
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; })
      .attr("r", function(d) { return Math.sqrt(d.size) / 10 || 4.5; })
      .style("fill", color)
      .on("click", click)
      .call(force.drag);
});

force.on("tick", function() {
  link.attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  node.attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; });
    });

// Color leaf nodes orange, and packages white or blue.

//function color(d) {
//  return d._children ? "#3182bd" : d.children ? "#c6dbef" : "#fd8d3c";
//}

// Toggle children on click.
force.on("click", function() {
  if (!d3.event.defaultPrevented) {
    if (d.children) {
      d._children = d.children;
      d.children = null;
    } else {
      d.children = d._children;
      d._children = null;
    }
    update();
  }
});

// Returns a list of all nodes under the root.
//function flatten(root) {
//  var nodes = [], i = 0;

//  function recurse(node) {
//    if (node.children) node.children.forEach(recurse);
//    if (!node.id) node.id = ++i;
//    nodes.push(node);
//  }

//  recurse(root);
//  return nodes;
//}
}

  });
  Shiny.outputBindings.register(networkOutputBinding, 'trestletech.networkbinding');

</script>